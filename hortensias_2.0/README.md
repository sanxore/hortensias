// @auteur Karim Baïna, ENSIAS, Décembre 2010, updated Décembre 2018

hortensias is a pedagocial language coming with two components hensiasc and hensiasi
hensiasc is a pedagocial compiler of hortensias language towards simple one-address code
hensiasi a pedagocial interpreter of the generated one-address code
hensiasc and hensiasi are all written in C language.

by pedagogical, I mean three things :

- hortensias is a simple language for programming beginners

- hortensias comes with multilangual (for the moment : english, spanish, french, and german) error messages for non english speaking students

- hortensias is a pedagogical platform for learning how to build a LL(1) top down compiler, pseudo code generator, optimiser and pseudocode interpreter

hortensias name is inspired from flowers name, but also contain ensias prefix which is Ecole Nationale Supérieure d'Informatique et d'Analyse des Systèmes, Mohammed V University in Rabat, Morocco where hortensias is built.

I invite you to first read LL1_hortensias_grammar.txt to learn about hortensias language LL(1) grammar.
I invite you to discover test directory containing some hortensias program to begin with.

make all : compiles both the compiler & the interpreter
./hensiasc < f.hos : launches the compiler on f.hos hostensias program
./hensiasi : launches the interpreter
./hensiasc < f.hos | ./hensiasi
good discovery.

Prof. Karim Baïna
ENSIAS
Mohammed V University in Rabat, Morocco


