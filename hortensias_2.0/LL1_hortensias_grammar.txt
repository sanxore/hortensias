// @ auteur Karim Baïna, ENSIAS, Décembre 2010 update Décembre 2018

Pragma :

#English 	// switch to english error handler (default one)
#French		// switch to french error handler
#Spanish	// switch to spanish error handler
#German		// switch to german error handler
#optimise	// activate optimiser (by default the optimiser is not active)
#rightassoc 	// select a semantic analyser with right AST
#lefttassoc 	// select a semantic analyser with left AST

a pragma may cancel the effect a previous pragma
(a pragma may occur multiple times without verification)

Syntax :
 PRE_PROG : LISTE_PRAGMA PROG

 LISTE_PRAGMA : PRAGMA LISTE_PRAGMA | epsilon

 PROG : LISTE_DECL begin LISTE_INST end

 LISTE_INST : INST LISTE_INSTAUX

 LISTE_INSTAUX : LISTE_INST  | epsilon

 LISTE_DECL : DECL LISTE_DECLAUX 
 
 LISTE_DECLAUX : LISTE_DECL | epsilon
 
 DECL : idf TYPE DECL_AUX
 
 DECL_AUX : CONST ';' | ';'
 
 TYPE : int | bool | double | string
 
 CONST : inumber | dnumber | cstring | true | false
 
 INST : IDF = ADDSUB ';'
      | IDF = TRUE ';'
      | IDF = FALSE ';'
      | if ‘(‘ idf == ADDSUB ‘)’ then LISTE_INST IF_INSTAUX 
      | print IDF ';' | print cstring ;
      | for IDF = inumber to inumber do LISTE_INST endfor
      | switch ( IDF ) SWITCH_BODY default : LISTE_INST break ; endswitch 

 SWITCH_BODY : case inumber : LIST_INST break ;  SWITCH_BODYAUX

 SWITCH_BODYAUX : SWITCH_BODY | epsilon

 IF_INSTAUX :  endif  | else LISTE_INST endif

 ADDSUB : MULT ADDSUBAUX

 ADDSUBAUX : – MULTDIV ADDSUBAUX
 ADDSUBAUX : + MULTDIV ADDSUBAUX
 ADDSUBAUX : epsilon

 MULTDIV: AUX MULTDIVAUX

 MULTDIVAUX : * AUX MULTDIVAUX
 MULTDIVAUX : / AUX MULTDIVAUX
 MULTDIVAUX : epsilon

 AUX : idf
 AUX : inumber | dnumber
 AUX : ( ADDSUB )
