%{
// @ auteur Karim Baïna, ENSIAS, Décembre 2010 update Décembre 2018
#include "analyseur_hortensias.h"
#include "i18n.h"
#define debug false
static int my_yylineno = 1;
%}
%option noyywrap
%%
#English 		{if (debug) printf ("#English\n"); set_LANGUE_COURANTE(English); return PRAGMA;}
#French			{if (debug) printf ("#French\n"); set_LANGUE_COURANTE(French); return PRAGMA;}
#Spanish		{if (debug) printf ("#Spanish\n"); set_LANGUE_COURANTE(Spanish); return PRAGMA;}
#German			{if (debug) printf ("#German\n"); set_LANGUE_COURANTE(German); return PRAGMA;}
#optimise		{if (debug) printf ("#optimise\n"); set_Mode_Optimisation( true ); return PRAGMA;}
#rightassoc 		{if (debug) printf ("#rightassoc\n"); set_Left_Associativity( false ) ; return PRAGMA;}
#leftassoc 		{if (debug) printf ("#lefttassoc\n"); set_Left_Associativity( true ) ; return PRAGMA;}
\"[^"]*\"	      {if (debug) printf ("CSTRING\n"); set_string_attributes( yytext ); return CSTRING;}
";" 			{if (debug) printf (";\n"); return PVIRG;}
"int"/[^a-z0-9] 			{if (debug) printf ("INT\n"); return INT;}
"bool"/[^a-z0-9] 			{if (debug) printf ("BOOL\n"); return BOOL;}
"double"/[^a-z0-9] 		{if (debug) printf ("DOUBLE\n"); return DOUBLE;}
"string"/[^a-z0-9] 		{if (debug) printf ("STRING\n"); return STRING;}
"true"/[^a-z0-9] 			{if (debug) printf ("TRUE\n"); set_number_attributes(1.0); return TRUE;}
"false"/[^a-z0-9] 		{if (debug) printf ("FALSE\n"); set_number_attributes(0.0); return FALSE;}
"begin"/[^a-z0-9]			{if (debug) printf ("BEGIN\n"); return BEG_IN;}
"end"/[^a-z0-9]			{if (debug) printf ("END\n"); return END;}
"if"/[^a-z0-9]			{if (debug) printf ("IF\n"); return IF;}
"then"/[^a-z0-9]			{if (debug) printf ("THEN\n"); return THEN;}
"else"/[^a-z0-9]			{if (debug) printf ("ELSE\n"); return ELSE;}
"endif"/[^a-z0-9]			{if (debug) printf ("ENDIF\n"); return ENDIF;}
"print"/[^a-z0-9]			{if (debug) printf ("PRINT\n"); return PRINT;}
"for"/[^a-z0-9] 			{if (debug) printf ("FOR\n"); return FOR;}
"to"/[^a-z0-9] 			{if (debug) printf ("TO\n"); return TO;}
"do"/[^a-z0-9] 			{if (debug) printf ("DO\n"); return DO;}
"endfor"/[^a-z0-9]		{if (debug) printf ("ENDFOR\n"); return ENDFOR;}
"switch"/[^a-z0-9]		{if (debug) printf ("SWITCH\n"); return SWITCH;}
"case"/[^a-z0-9]		{if (debug) printf ("CASE\n"); return CASE;}
"break"/[^a-z0-9]		{if (debug) printf ("BREAK\n"); return BREAK;}
"default"/[^a-z0-9]		{if (debug) printf ("DEFAULT\n"); return DEFAULT;}
"endswitch"/[^a-z0-9]		{if (debug) printf ("ENDSWITCH\n"); return ENDSWITCH;}
":" 			{if (debug) printf ("DEUXPOINT\n"); return DEUXPOINT;}

"("			{if (debug) printf ("(\n"); return POPEN;}
")"			{if (debug) printf (")\n"); return PCLOSE;}


"add"		 	{if (debug) printf ("ADD\n"); /* MOT CELF RESERVE 1-ADDRESS CODE (EXCEPTE LE PRINT COMMUN DE ZZ et 1-ADDRESS CODE) ANTICIPATION DES ERREURS SYNTAXIQUES DE LA VM */ return ERRORLEX;}
"idiv"		{if (debug) printf ("IDIV\n"); /* MOT CELF RESERVE 1-ADDRESS CODE (EXCEPTE LE PRINT COMMUN DE ZZ et 1-ADDRESS CODE) ANTICIPATION DES ERREURS SYNTAXIQUES DE LA VM */ return ERRORLEX;}
"ddiv"		{if (debug) printf ("IDIV\n"); /* MOT CELF RESERVE 1-ADDRESS CODE (EXCEPTE LE PRINT COMMUN DE ZZ et 1-ADDRESS CODE) ANTICIPATION DES ERREURS SYNTAXIQUES DE LA VM */ return ERRORLEX;}

"dupl" 			{if (debug) printf ("DUPL\n"); /* MOT CELF RESERVE 1-ADDRESS CODE (EXCEPTE LE PRINT COMMUN DE ZZ et 1-ADDRESS CODE) ANTICIPATION DES ERREURS SYNTAXIQUES DE LA VM */ return ERRORLEX;}
"jmp" 			{if (debug) printf ("JMP\n"); /* MOT CELF RESERVE 1-ADDRESS CODE (EXCEPTE LE PRINT COMMUN DE ZZ et 1-ADDRESS CODE) ANTICIPATION DES ERREURS SYNTAXIQUES DE LA VM */ return ERRORLEX;}
"jne" 			{if (debug) printf ("JNE\n"); /* MOT CELF RESERVE 1-ADDRESS CODE (EXCEPTE LE PRINT COMMUN DE ZZ et 1-ADDRESS CODE) ANTICIPATION DES ERREURS SYNTAXIQUES DE LA VM */ return ERRORLEX;}
"jg" 			{if (debug) printf ("JG\n"); /* MOT CELF RESERVE 1-ADDRESS CODE (EXCEPTE LE PRINT COMMUN DE ZZ et 1-ADDRESS CODE) ANTICIPATION DES ERREURS SYNTAXIQUES DE LA VM */ return ERRORLEX;}
"load" 			{if (debug) printf ("LOAD\n"); /* MOT CELF RESERVE 1-ADDRESS CODE (EXCEPTE LE PRINT COMMUN DE ZZ et 1-ADDRESS CODE) ANTICIPATION DES ERREURS SYNTAXIQUES DE LA VM */ return ERRORLEX;}
"mult" 			{if (debug) printf ("MULT\n"); /* MOT CELF RESERVE 1-ADDRESS CODE (EXCEPTE LE PRINT COMMUN DE ZZ et 1-ADDRESS CODE) ANTICIPATION DES ERREURS SYNTAXIQUES DE LA VM */ return ERRORLEX;}
"push" 			{if (debug) printf ("PUSH\n"); /* MOT CELF RESERVE 1-ADDRESS CODE (EXCEPTE LE PRINT COMMUN DE ZZ et 1-ADDRESS CODE) ANTICIPATION DES ERREURS SYNTAXIQUES DE LA VM */ return ERRORLEX;}
"sub" 			{if (debug) printf ("SUB\n"); /* MOT CELF RESERVE 1-ADDRESS CODE (EXCEPTE LE PRINT COMMUN DE ZZ et 1-ADDRESS CODE) ANTICIPATION DES ERREURS SYNTAXIQUES DE LA VM */ return ERRORLEX;}
"store"			{if (debug) printf ("STORE\n"); /* MOT CELF RESERVE 1-ADDRESS CODE (EXCEPTE LE PRINT COMMUN DE ZZ et 1-ADDRESS CODE) ANTICIPATION DES ERREURS SYNTAXIQUES DE LA VM */ return ERRORLEX;}
"swap" 			{if (debug) printf ("SWAP\n"); /* MOT CELF RESERVE 1-ADDRESS CODE (EXCEPTE LE PRINT COMMUN DE ZZ et 1-ADDRESS CODE) ANTICIPATION DES ERREURS SYNTAXIQUES DE LA VM */ return ERRORLEX;}

[a-zA-Z_][a-zA-Z0-9_]* 	{
			if (debug) printf ("IDF(%s)\n",yytext);
			set_idf_attributes(yytext, my_yylineno);
			return IDF;
			}
[0-9]+[.][0-9]+		{
			if (debug) printf ("NUMBER(%s)\n",yytext);
			set_number_attributes(atof(yytext));
			return DNUMBER;
			}
[0-9]+			{
			if (debug) printf ("NUMBER(%s)\n",yytext);
			set_number_attributes((double)atoi(yytext));
			return INUMBER;
			}
"=="			{if (debug) printf ("=="); return EQEQ;}
"="			{if (debug) printf ("="); return EQ;}
"+"			{if (debug) printf ("+"); return PLUS;}
"-"			{if (debug) printf ("-"); return MINUS;}
"*"			{if (debug) printf ("*"); return MULT;}
"/"			{if (debug) printf ("/"); return DIV;}
[R][E][M][^\n]*     /* commentaires */
[ \t]		        /* caractère vide */
\n                    {my_yylineno++; set_token_attributes( my_yylineno );}  
.			{return ERRORLEX;}
%%
