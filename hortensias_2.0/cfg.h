#ifndef CFG_H
#define CFG_H

#include "ast.h"

// @auteur Karim Ba�na, ENSIAS, D�cembre 2010, updated D�cember 2018

#define debug false

#define NULL ((void *)0)

typedef enum {DBL, STR} elementType;

typedef union{
	char * svalue;
	double dvalue;
} elementValue;


typedef struct Elt {
	elementType etype;
	elementValue evalue;
} Element;


typedef enum {PrintIdf, PrintString, AssignArith, AssignBool, IfThenArith, IfThenElseArith, For, Switch} Type_INST ;

typedef struct {
  char *name;      
  int  nbdecl;     
  Type typevar;    
  boolean initialisation; // attribut n�cessaire � l'analyseur s�mantique 
  Element valinit;      
  boolean utilisation; // attribut n�cessaire � l'optimisateur de code (la non utilisation n'est pas boquante pour l'analyseur s�mantique)
  int line; 
} varvalueType;

typedef struct {
  Type typename;    // CONST_IB.typename
  double valinit;      // CONST_IB.valinit
} constvalueType;

typedef struct {
  char *value;
} stringvalueType;

typedef struct {
  int line;  //TOKEN.line
} tokenvalueType;

typedef struct {
  Type typename;    // TYPE.typename
} typevalueType;

struct INST; // pr� d�claration de la structure de stockage d'une instruction

struct LIST_INST;// pr� d�claration de la structure de stockage d'une liste d'instruction

struct CASE_BODY; // pr� d�claration de la structure de stockage d'un case de switch

typedef struct INST {
  Type_INST typeinst;
  union  {
    // PRINT idftoprint
    struct  {
      int rangvar; // cas (print IDF) indice de l'idf, � afficher, dans la table des symboles
      char * str; // cas (print STRING)
    } printnode;
    // left := right
    struct  {
      int rangvar; // indice de l'idf, o� il faut affecter, dans la table des symboles
      //int right0; // la valeur boolenne � affecter // a nettoyer
      AST right; // l'expression � affecter (arithm�tique ou bool�enne)
    } assignnode;
    // IF ... THEN 
    struct  {
      int rangvar; // indice de l'idf � comparer, dans la table des symbole
      //double right; // la valeur de comparaison
      AST right; // l'expression � comparer
      struct LIST_INST * thenlinst; // then list of instructions
      struct LIST_INST * elselinst; // else list of instructions
    } ifnode;
      // for (index:= exp_min..exp_max) loop list_inst end loop;
    struct {
      int rangvar; // indice de l'index de la boucle
      int borneinf; // l'expression borne inf
      int bornesup; // l'expression borne sup
      struct LIST_INST * forbodylinst; // for body list of instructions
    } fornode;
      // switch ( x ) case 1 : ... break ; case 20 : ... break ; .... default : ... break ; endswitch
    struct {
	int rangvar; // indice de la variable du switch
	int nbcases;
	struct CASE_BODY *cases ; // pour les cases (SWITCH_BODY), tableau dynamique non tri� de couples val- liste
	struct LIST_INST * defaultbodylinst ; // la liste d'instructions par d�faut du switch
    } switchnode ;
  } node;
} instvalueType;


typedef struct CASE_BODY {
	int value ; // la valeur du cas (doit �tre >= 0)
	struct LIST_INST * casebodylinst; // la liste d'instructions du cas
} casevaluelinstType;


typedef struct LIST_INST {
  struct INST first;
  struct LIST_INST * next;
} listinstvalueType;

typedef union {
  varvalueType varattribute;            //IDF.varattribute
  constvalueType constattribute;        // CONST_IB.constattribute
  Type typename;                        // TYPE.typename
  instvalueType instattribute;          // INST.instattribute
  listinstvalueType listinstattribute;  // LIST_INST.listinstattribute
  //AST expattribute;                     // EXP.expattribute
} valueType;

//#define YYSTYPE valueType

extern instvalueType* creer_instruction_printIdf(int rangvar);
extern instvalueType* creer_instruction_printString(char * s);
extern instvalueType* creer_instruction_affectation(int rangvar, AST * past);
//extern instvalueType* creer_instruction_if(int rangvar, constvalueType constattribute, listinstvalueType * plistthen, listinstvalueType * plistelse);
extern instvalueType* creer_instruction_if(int rangvar, AST * past, listinstvalueType * plistthen, listinstvalueType * plistelse);
extern instvalueType* creer_instruction_for(int rangvar, int borneinf, int bornesup, listinstvalueType *plistfor);
extern instvalueType* creer_instruction_switch(int rangvar, int nbcases, casevaluelinstType *cases, listinstvalueType *plistswitchdefaultbody);


extern void inserer_inst_en_queue(listinstvalueType * listinstattribute, instvalueType instattribute);

extern void inserer_inst_en_tete(listinstvalueType ** listinstattribute, instvalueType instattribute);

extern void afficher_inst(instvalueType instattribute);

extern void afficher_list_inst(listinstvalueType * plistinstattribute);

// extern void interpreter_inst(instvalueType instattribute);

// extern void interpreter_list_inst(listinstvalueType * listinstattribute);

extern double dvalue(Element e);

// pr�-requis : e.etype == STR;
extern char * svalue(Element e);

extern void setdvalue(Element *e, double d);

extern void setsvalue(Element *e, char * s);

#endif
