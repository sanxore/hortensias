#include "cfg.h"
#include "tablesymb.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define debug false

// @auteur Karim Baïna, ENSIAS, Décembre 2010, updated Décembre 2018

instvalueType* creer_instruction_printIdf(int rangvar){
        if (debug) printf("creer_instruction_printIdf()\n");

	instvalueType * printinstattribute = (instvalueType *) malloc (sizeof(instvalueType));

	printinstattribute->typeinst = PrintIdf;

	printinstattribute->node.printnode.rangvar = rangvar;
	
	if (debug) printf("out of creer_instruction_printIdf()\n");

	return printinstattribute;

}

instvalueType* creer_instruction_printString(char * s){
        if (debug) printf("creer_instruction_printString()\n");

	instvalueType * printinstattribute = (instvalueType *) malloc (sizeof(instvalueType));

	printinstattribute->typeinst = PrintString;

	printinstattribute->node.printnode.str = (char *) malloc ((strlen(s)+1)*sizeof(char));

	strcpy(printinstattribute->node.printnode.str, s);
	
	if (debug) printf("out of creer_instruction_printString()\n");

	return printinstattribute;
}

instvalueType* creer_instruction_affectation(int rangvar, AST * past){
        if (debug) 	printf("creer_instruction_affectation()\n");

	instvalueType * pinstattribute = (instvalueType *) malloc (sizeof(instvalueType));

	pinstattribute->typeinst = (type(*past)==Bool)?AssignBool:AssignArith;
	pinstattribute->node.assignnode.rangvar = rangvar;
	pinstattribute->node.assignnode.right = * past;

        if (debug) printf("out of creer_instruction_affectation()\n");

	return pinstattribute;
}


//instvalueType* creer_instruction_if(int rangvar, constvalueType constattribute, listinstvalueType * plistthen, listinstvalueType * plistelse){
instvalueType* creer_instruction_if(int rangvar, AST * past, listinstvalueType * plistthen, listinstvalueType * plistelse){

        if (debug) printf("creer_instruction_if()\n");

	instvalueType * pinstattribute = (instvalueType *) malloc (sizeof(instvalueType));

	pinstattribute->typeinst = ((plistelse != NULL)?IfThenElseArith:IfThenArith);
	pinstattribute->node.ifnode.rangvar = rangvar;
	pinstattribute->node.ifnode.right = * past;
	pinstattribute->node.ifnode.thenlinst = plistthen;
	pinstattribute->node.ifnode.elselinst = plistelse;

        if (debug) printf("out of creer_instruction_if()\n");	

	return pinstattribute;
}

instvalueType* creer_instruction_for(int rangvar, int borneinf, int bornesup, listinstvalueType *plistfor){

        if (debug) printf("creer_instruction_for()\n");

	instvalueType * pinstattribute = (instvalueType *) malloc (sizeof(instvalueType));

	pinstattribute->typeinst = For;
	pinstattribute->node.fornode.rangvar = rangvar;
	pinstattribute->node.fornode.borneinf = borneinf;
	pinstattribute->node.fornode.bornesup = bornesup;
	pinstattribute->node.fornode.forbodylinst = plistfor;

        if (debug) printf("out of creer_instruction_for()\n");	

	return pinstattribute;
}

/*
    struct {
	int rangvar; // indice de la variable du switch
	struct CASE_BODY *cases ; // pour les cases (SWITCH_BODY), tableau dynamique non trié de couples val- liste
	struct LIST_INST * defaultbodylinst ; // la liste d'instructions par défaut du switch
    } switchnode ;

typedef struct CASE_BODY {
	int value ; // la valeur du cas (doit être >= 0)
	struct LIST_INST * casebodylinst; // la liste d'instructions du cas
} casevaluelinstType;
*/

instvalueType* creer_instruction_switch(int rangvar, int nbcases, casevaluelinstType *cases, listinstvalueType *plistswitchdefaultbody){
        if (debug) printf("creer_instruction_switch()\n");

	instvalueType * pinstattribute = (instvalueType *) malloc (sizeof(instvalueType));

	pinstattribute->typeinst = Switch;
	pinstattribute->node.switchnode.rangvar = rangvar;
	pinstattribute->node.switchnode.nbcases = nbcases;
	pinstattribute->node.switchnode.cases = cases;
	pinstattribute->node.switchnode.defaultbodylinst = plistswitchdefaultbody;

        if (debug) printf("out of creer_instruction_switch()\n");	

	return pinstattribute;
}

/*
void interpreter_inst(instvalueType instattribute){
  double rexp;
  switch(instattribute.typeinst){
  case PrintIdf :
    if (debug) printf("Print");
    if (typevar(instattribute.node.printnode.rangvar) == Bool){
      printf("%s\n", ((valinit(instattribute.node.printnode.rangvar)==false)?"false":"true"));
    }else{
      printf("%lf\n", valinit(instattribute.node.printnode.rangvar));
    }
    break;
    
  case AssignArith :
   
    if (debug) printf("AssignArith");
    rexp=evaluer(instattribute.node.assignnode.right);
    if (debug) printf("rexp==%f\n",rexp);
    set_valinit(instattribute.node.assignnode.rangvar, rexp);
    if (debug) {
      printf("lexp==%f\n",valinit(instattribute.node.assignnode.rangvar));
      afficherTS();}
    break;
    
  case AssignBool :
    if (debug) printf("AssignBool");
   
    set_valinit(instattribute.node.assignnode.rangvar, instattribute.node.assignnode.right0);
   
    break;
    
  case IfThenArith :
    if (debug) printf("If");
    if ( valinit(instattribute.node.ifnode.rangvar) == evaluer(instattribute.node.ifnode.right) ){
      interpreter_list_inst( instattribute.node.ifnode.thenlinst );
    }
    break;

 case IfThenElseArith :
    if (debug) printf("If");
    if ( valinit(instattribute.node.ifnode.rangvar) == evaluer(instattribute.node.ifnode.right) ){
      interpreter_list_inst( instattribute.node.ifnode.thenlinst );
    }else{
      interpreter_list_inst( instattribute.node.ifnode.elselinst );
    }
    break;
  }
}

void interpreter_list_inst(listinstvalueType * plistinstattribute){
  if (debug) printf("here");
  if  (plistinstattribute != NULL){
    interpreter_inst(plistinstattribute->first);
    interpreter_list_inst(plistinstattribute->next);
  } 
}

*/

void afficher_inst(instvalueType instattribute){
  if (debug) printf("afficher_inst()\n");

  switch(instattribute.typeinst){

  case PrintIdf :
    printf("PrintIdf "); printf("%s ;\n", name(instattribute.node.printnode.rangvar));
    break;

  case PrintString :
    printf("PrintString "); printf("%s ;\n", instattribute.node.printnode.str);
    break;
        
  case AssignArith :
    printf("AssignArith ");
    printf("%s", name(instattribute.node.assignnode.rangvar));
    afficher_infixe_arbre(instattribute.node.assignnode.right);
    printf(";\n");
    break;

  case AssignBool :
    printf("AssignBool ");
    printf("%s", name(instattribute.node.assignnode.rangvar));
    afficher_infixe_arbre( instattribute.node.assignnode.right );
    // printf(" := %s",(instattribute.node.assignnode.right0==true)?"true":"false");
    printf(";\n");
    break;
    
  case IfThenArith :
    printf("If ");
    printf("( %s ==",name(instattribute.node.ifnode.rangvar));
    // printf("%lf )", instattribute.node.ifnode.right);
    afficher_infixe_arbre( instattribute.node.ifnode.right );
    printf(" )\n Then ");
    afficher_list_inst( instattribute.node.ifnode.thenlinst );
    printf(" endIf\n");
    break;

 case IfThenElseArith :
   printf("If ");
   printf("( %s == ",name(instattribute.node.ifnode.rangvar));
   // printf("%lf )", instattribute.node.ifnode.right);
   afficher_infixe_arbre( instattribute.node.ifnode.right );
   printf(" )\n Then ");
   afficher_list_inst( instattribute.node.ifnode.thenlinst );
   printf(" Else ");
   afficher_list_inst( instattribute.node.ifnode.elselinst );
   printf(" endIf\n");
   break;

 case For :
   printf("For ");
   printf("( %s = ",name(instattribute.node.fornode.rangvar));
   printf(" %d to ",instattribute.node.fornode.borneinf );
   printf(" %d ) ",instattribute.node.fornode.bornesup );

   afficher_list_inst( instattribute.node.fornode.forbodylinst );
   printf(" endFor\n");
   break;

 case Switch :
   printf("Switch ");
   printf("( %s ) ",name(instattribute.node.switchnode.rangvar));


   int i = 0;
   printf("----> normalmente hay %d cases + default\n",instattribute.node.switchnode.nbcases );
   while (i < instattribute.node.switchnode.nbcases ) {
	   printf("case %d : ",instattribute.node.switchnode.cases[i].value);
	   afficher_list_inst( instattribute.node.switchnode.cases[i].casebodylinst );
   	i++;
   }

   printf(" default : ");
   afficher_list_inst( instattribute.node.switchnode.defaultbodylinst );

   printf(" endSwitch\n");
   break;

  }



  if (debug) printf("out of afficher_inst()\n");
}

void afficher_list_inst(listinstvalueType * plistinstattribute){
  if (debug) printf("afficher_list_inst()\n");

  if (plistinstattribute != NULL){
   
    afficher_inst(plistinstattribute->first);    
    afficher_list_inst(plistinstattribute->next);
  }else if (debug) printf("Bugg list instruction NULL !!!!!!!!!!!!!!!!!!!!!!!\n");

  if (debug) printf("out of afficher_list_inst()\n");

}

void inserer_inst_en_tete(listinstvalueType ** pplistinstattribute, instvalueType instattribute){
//int i=0; for (i=0;i<100;i++)printf("IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII\n");

  if (debug) {printf("inserer_inst_en_tete( "); afficher_list_inst(*pplistinstattribute); afficher_inst(instattribute); printf(" )");}

  listinstvalueType * liste = (listinstvalueType *) malloc(sizeof(listinstvalueType));
//printf("MALLOCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC");
  liste->first = instattribute;
  liste->next = *pplistinstattribute;

  *pplistinstattribute = liste;

  if (debug) printf("out of inserer_inst_en_tete()\n");
}

void inserer_inst_en_queue(listinstvalueType * plistinstattribute, instvalueType instattribute){
  if (debug) printf ("debut inserer_inst_en_queue()");

  listinstvalueType * liste = (listinstvalueType *) malloc(sizeof(listinstvalueType));
  liste->first = instattribute;
  liste->next = NULL;

  if (plistinstattribute->next == NULL) {
    plistinstattribute->next = liste;
  }else{
    listinstvalueType * pliste = plistinstattribute;
    
    while(pliste->next != NULL) {
      pliste = pliste->next;
    }
    
    pliste->next = liste;
  }
  if (debug) printf ("debut inserer_inst_en_queue()");
}



// pré-requis : e.etype == DBL;
double dvalue(Element e){
	return e.evalue.dvalue;
}

// pré-requis : e.etype == STR;
char * svalue(Element e){
	return e.evalue.svalue;
}

void setdvalue(Element *e, double d){
	e->evalue.dvalue = d;
	e->etype = DBL;
}

void setsvalue(Element *e, char * s){
	if (debug) printf("setsvalue()\n");
	if ((debug) && (s == NULL)) printf("s NULL!!!!!!!\n"); 
	e->evalue.svalue = (char *)malloc((strlen(s)+1)*sizeof(char));
	strcpy(e->evalue.svalue, s);
	e->etype = STR;
	if (debug) printf("out of setsvalue()\n");
}
